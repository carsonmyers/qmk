#include "ergodox_ez.h"
#include "debug.h"
#include "action_layer.h"
#include "version.h"

#define BASE 0    // default layer
#define SHFT 1    // shift layer
#define CIRC 2    // circumflex dead key
#define S_CIRC 3  // shifted circumflex dead key
#define ACUT 4    // acute dead key
#define S_ACUT 5  // shifted acute dead key
#define GRAV 6    // grave dead key
#define S_GRAV 7  // shifted grave acute key
#define TREM 8    // trema dead key
#define S_TREM 9  // shifted trema dead key
#define BREV 10   // breve dead key
#define S_BREV 11 // shifted breve dead key
#define SYMB 12   // symbols and numbers
#define FUN  13   // function keys
#define GAME 14   // simplified gaming layout
#define MVMT 15   // movement layer

#define EPRM  1 // Macro 1: Reset EEPROM
#define RESET 2 // Macro 2: Reset OSL

#define a_ACU UC(0x00E1)
#define A_ACU UC(0x00C1)
#define a_BRV UC(0x0103)
#define A_BRV UC(0x0102)
#define a_CIR UC(0x00E2)
#define A_CIR UC(0x00C2)
#define a_GRV UC(0x00E0)
#define A_GRV UC(0x00C0)
#define a_RNG UC(0x00E5)
#define A_RNG UC(0x00C5)
#define a_TRM UC(0x00E4)
#define A_TRM UC(0x00C4)
#define c_ACU UC(0x0107)
#define C_ACU UC(0x0106)
#define c_CED UC(0x00E7)
#define C_CED UC(0x00C7)
#define c_CIR UC(0x0109)
#define C_CIR UC(0x0108)
#define e_ACU UC(0x00E9)
#define E_ACU UC(0x00C9)
#define e_BRV UC(0x0115)
#define E_BRV UC(0x0114)
#define e_CIR UC(0x00EA)
#define E_CIR UC(0x00CA)
#define e_GRV UC(0x00E8)
#define E_GRV UC(0x00C8)
#define e_TRM UC(0x00EB)
#define E_TRM UC(0x00CB)
#define g_BRV UC(0x011F)
#define G_BRV UC(0x011E)
#define g_CIR UC(0x011D)
#define G_CIR UC(0x011C)
#define h_CIR UC(0x0125)
#define H_CIR UC(0x0124)
#define i_ACU UC(0x00ED)
#define I_ACU UC(0x00CD)
#define i_BRV UC(0x012D)
#define I_BRV UC(0x012C)
#define i_CIR UC(0x00EE)
#define I_CIR UC(0x00CE)
#define i_GRV UC(0x00EC)
#define I_GRV UC(0x00CC)
#define i_TRM UC(0x00EF)
#define I_TRM UC(0x00CF)
#define j_CIR UC(0x0135)
#define J_CIR UC(0x0134)
#define l_ACU UC(0x013A)
#define L_ACU UC(0x0139)
#define n_ACU UC(0x0144)
#define N_ACU UC(0x0143)
#define n_TLD UC(0x00F1)
#define N_TLD UC(0x00D1)
#define o_ACU UC(0x00F3)
#define O_ACU UC(0x00D3)
#define o_BRV UC(0x014F)
#define O_BRV UC(0x014E)
#define o_CIR UC(0x00F4)
#define O_CIR UC(0x00D4)
#define o_GRV UC(0x00F2)
#define O_GRV UC(0x00D2)
#define o_TRM UC(0x00F6)
#define O_TRM UC(0x00D6)
#define r_ACU UC(0x0155)
#define R_ACU UC(0x0154)
#define s_ACU UC(0x015B)
#define S_ACU UC(0x015A)
#define s_CIR UC(0x015D)
#define S_CIR UC(0x015C)
#define u_ACU UC(0x00FA)
#define U_ACU UC(0x00DA)
#define u_BRV UC(0x016D)
#define U_BRV UC(0x016C)
#define u_CIR UC(0x00FB)
#define U_CIR UC(0x00DB)
#define u_GRV UC(0x00F9)
#define U_GRV UC(0x00D9)
#define u_TRM UC(0x00FC)
#define U_TRM UC(0x00DC)
#define w_CIR UC(0x0175)
#define W_CIR UC(0x0174)
#define y_ACU UC(0x00FD)
#define Y_ACU UC(0x00DD)
#define y_CIR UC(0x0177)
#define Y_CIR UC(0x0176)

#define GRAVE UC(0x0300)
#define ACUTE UC(0x0301)
#define CIRCM UC(0x0302)
#define TILDE UC(0x0303)
#define TREMA UC(0x0308)
#define RING  UC(0x030A)
#define CEDIL UC(0x0327)

#define LSANG UC(0x2039)
#define RSANG UC(0x203A)
#define LDANG UC(0x00AB)
#define RDANG UC(0x00BB)
#define LSQUO UC(0x2018)
#define RSQUO UC(0x2019)
#define LDQUO UC(0x201C)
#define RDQUO UC(0x201D)
#define SLQUO UC(0x201A)
#define DLQUO UC(0x201E)

#define INVEX UC(0x00A1)
#define INVQU UC(0x00BF)
#define GBPND UC(0x00A3)
#define EURO  UC(0x20AC)
#define DEGRE UC(0x00B0)
#define EMDAS UC(0x2014)
#define BULLT UC(0x2022)
#define ELLPS UC(0x2026)

#define WINMX LCTL(LALT(LGUI(KC_M)))
#define WINCN LCTL(LALT(LGUI(KC_C)))
#define WINLF LCTL(LALT(LGUI(KC_LEFT)))
#define WINRT LCTL(LALT(LGUI(KC_RIGHT)))
#define SCRLF LCTL(LALT(KC_LEFT))
#define SCRRT LCTL(LALT(KC_RIGHT))

#define WLEFT LALT(KC_LEFT)
#define WRGHT LALT(KC_RIGHT)
#define LSTRT LGUI(KC_LEFT)
#define LEND  LGUI(KC_RIGHT)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/* Keymap 0: Basic layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |    `   |   !  |   @  | CIRC | ACUT | GRAV |   °  |           |  Del |    ^ | TREM |   *  |   '  |   =  |  Bksp  |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |   Tab  |   q  |   w  |   e  |   r  |   t  |   \  |           |   -  |   y  |   u  |   i  |   o  |   p  |    å   |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * | Ex/SYM |   a  |   s  |   d  |   f  |   g  |------|           |------|   h  |   j  |   k  |   l  |   ;  |  SYMB  |
 * |--------+------+------+------+------+------|   \  |           |   /  |------+------+------+------+------+--------|
 * |  SHFT  |   z  |   x  |   c  |   v  |   b  |      |           |      |   n  |   m  |   ,  |    . |   ?  |  SHFT  |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   | MVMT | LAlt |   ‹  |   ›  |   ç  |                                       |   ñ  |   ‘  |   ’  |    ‚ | MVMT |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        | Prev | Next |       | VolD | VolU |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      | Play |       | Mute |      |      |
 *                                 |  Spc | LCmd |------|       |------| RCmd | Spc  |
 *                                 |  Ctl |      | Game |       | BREV |  Ret | Ctl  |
 *                                 `--------------------'       `--------------------'
 */
// If it accepts an argument (i.e, is a function), it doesn't need KC_.
// Otherwise, it needs KC_*
[BASE] = LAYOUT_ergodox(  // layer 0 : default
    // left hand
    KC_GRAVE,           KC_EXLM,   KC_AT,     OSL(CIRC), OSL(ACUT), OSL(GRAV), DEGRE,
    KC_TAB,             KC_Q,      KC_W,      KC_E,      KC_R,      KC_T,      KC_BSLS,
    LT(SYMB, KC_ESC),   KC_A,      KC_S,      KC_D,      KC_F,      KC_G,
    LM(SHFT, MOD_LSFT), KC_Z,      KC_X,      KC_C,      KC_V,      KC_B,      KC_PIPE,
    MO(MVMT),           KC_LALT,   LSANG,     RSANG,    c_CED,

                                                 KC_MRWD,  KC_MFFD,
                                                           KC_MPLY,
                            MT(MOD_LCTL, KC_SPC), KC_LGUI, TG(GAME),
    // right hand
     KC_DEL,      KC_CIRC, OSL(TREM), KC_ASTR, KC_QUOT, KC_EQL,  KC_BSPC,
     KC_MINS,     KC_Y,    KC_U,      KC_I,    KC_O,    KC_P,    a_RNG,
                  KC_H,    KC_J,      KC_K,    KC_L,    KC_SCLN, MO(SYMB),
     KC_SLSH,     KC_N,    KC_M,      KC_COMM, KC_DOT,  KC_QUES, LM(SHFT, MOD_LSFT),
                           n_TLD,     LSQUO,   RSQUO,   SLQUO,   MO(MVMT),  
     KC_VOLD,   KC_VOLU,
     KC_MUTE,
     OSL(BREV), MT(MOD_RGUI, KC_ENT), MT(MOD_RCTL, KC_SPC)
),

    /* Keymap 1: Shift layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |    ~   |   €  |   #  | CIRC | ACUT | GRAV |   £  |           |  Del |   $  | TREM |   &  |   "  |   +  |  Bksp  |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |   Q  |   W  |   E  |   R  |   T  |   —  |           |   _  |   Y  |   U  |   I  |   O  |   P  |    Å   |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |  FUNC  |   A  |   S  |   D  |   F  |   G  |------|           |------|   H  |   J  |   K  |   L  |   :  |  FUNC  |
 * |--------+------+------+------+------+------|      |           |   |  |------+------+------+------+------+--------|
 * |        |   Z  |   X  |   C  |   V  |   B  |      |           |      |   N  |   M  |   …  |   ·  |   %  |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      | LAlt |   «  |   »  |   Ç  |                                       |   Ñ  |   ”  |   “  |   „  |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       | BREV |      |      |
 *                                 `--------------------'       `--------------------'
 */
[SHFT] = LAYOUT_ergodox(
    // left hand
    KC_TRNS,   EURO,     KC_HASH, OSL(S_CIRC), OSL(S_ACUT), OSL(S_GRAV), GBPND,
    KC_TRNS,   KC_TRNS,  KC_TRNS, KC_TRNS,     KC_TRNS,     KC_TRNS,     EMDAS,
    MO(FUN),   KC_TRNS,  KC_TRNS, KC_TRNS,     KC_TRNS,     KC_TRNS,
    KC_TRNS,   KC_TRNS,  KC_TRNS, KC_TRNS,     KC_TRNS,     KC_TRNS,     KC_TRNS,
    KC_NO,     KC_LALT,  LDANG,   RDANG,       C_CED,

                                                  KC_TRNS, KC_TRNS,
                                                           KC_TRNS,
                                         KC_TRNS, KC_TRNS, KC_TRNS,
    // right hand
     KC_DEL,    KC_DLR,  OSL(S_TREM), KC_AMPR, KC_DQUO, KC_PLUS, KC_BSPC,
     KC_UNDS,   KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, KC_TRNS, A_RNG,
                KC_TRNS, KC_TRNS,     KC_TRNS, KC_TRNS, KC_TRNS, MO(FUN),
     KC_PIPE,   KC_TRNS, KC_TRNS,     ELLPS,   BULLT,   KC_PERC, KC_TRNS,
                         N_TLD,       LDQUO,   RDQUO,   DLQUO,   KC_NO,  
     KC_TRNS,     KC_TRNS,
     KC_TRNS,
     OSL(S_BREV), KC_TRNS, KC_TRNS
),

/* Keymap 1: Circumflex layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |   ŵ  |   ê  |      |      |      |           |      |   ŷ  |   û  |   î  |   ô  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |   â  |   ŝ  |      |      |   ĝ  |------|           |------|   ĥ  |   ĵ  |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |  SHFT  |      |      |   ĉ  |      |      |      |           |      |      |      |      |      |      |  SHFT  |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[CIRC] = LAYOUT_ergodox(
    // left hand
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  w_CIR, e_CIR, RESET, RESET, RESET,
    RESET, a_CIR,  s_CIR, RESET, RESET, g_CIR,
    RESET, RESET,  RESET, c_CIR, RESET, RESET, RESET,
    RESET, RESET,  RESET, RESET, RESET,

                                              RESET,RESET,
                                                    RESET,
                                      RESET, RESET, RESET,
    // right hand
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, y_CIR, u_CIR, i_CIR, o_CIR, RESET, RESET,
            h_CIR, j_CIR, RESET, RESET, RESET, RESET,
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
                   RESET, RESET, RESET, RESET, RESET,  
     RESET, RESET,
     RESET,
     RESET, RESET, RESET
),

/* Keymap 1: Shifted Circumflex layer
 * 
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |   Ŵ  |   Ê  |      |      |      |           |      |   Ŷ  |   Û  |   Î  |   Ô  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |   Â  |   Ŝ  |      |      |   Ĝ  |------|           |------|   Ĥ  |   Ĵ  |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |   Ĉ  |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[S_CIRC] = LAYOUT_ergodox(
    // left hand
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  W_CIR, E_CIR, RESET, RESET, RESET,
    RESET, A_CIR,  S_CIR, RESET, RESET, G_CIR,
    RESET, RESET,  RESET, C_CIR, RESET, RESET, RESET,
    RESET, RESET,  RESET, RESET, RESET,

                                              RESET,RESET,
                                                    RESET,
                                      RESET, RESET, RESET,
    // right hand
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, Y_CIR, U_CIR, I_CIR, O_CIR, RESET, RESET,
            H_CIR, J_CIR, RESET, RESET, RESET, RESET,
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
                   RESET, RESET, RESET, RESET, RESET,  
     RESET, RESET,
     RESET,
     RESET, RESET, RESET
),

/* Keymap 1: Acute layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |   é  |   ŕ  |      |      |           |      |   ý  |   ú  |   í  |   ó  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |   á  |   ś  |      |      |      |------|           |------|      |      |      |   ĺ  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |  SHFT  |      |      |   ć  |      |      |      |           |      |   ń  |      |      |      |      |  SHFT  |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[ACUT] = LAYOUT_ergodox(
    // left hand
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, e_ACU, r_ACU, RESET, RESET,
    RESET, a_ACU,  s_ACU, RESET, RESET, RESET,
    RESET, RESET,  RESET, c_ACU, RESET, RESET, RESET,
    RESET, RESET,  RESET, RESET, RESET,

                                              RESET,RESET,
                                                    RESET,
                                      RESET, RESET, RESET,
    // right hand
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, y_ACU, u_ACU, i_ACU, o_ACU, RESET, RESET,
            RESET, RESET, RESET, l_ACU, RESET, RESET,
     RESET, n_ACU, RESET, RESET, RESET, RESET, RESET,
                   RESET, RESET, RESET, RESET, RESET,  
     RESET, RESET,
     RESET,
     RESET, RESET, RESET
),

/* Keymap 1: Shifted Acute layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |   É  |   Ŕ  |      |      |           |      |   Ý  |   Ú  |   Í  |   Ó  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |   Á  |   Ś  |      |      |      |------|           |------|      |      |      |   Ĺ  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |   Ć  |      |      |      |           |      |   Ń  |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[S_ACUT] = LAYOUT_ergodox(
    // left hand
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, E_ACU, R_ACU, RESET, RESET,
    RESET, A_ACU,  S_ACU, RESET, RESET, RESET,
    RESET, RESET,  RESET, C_ACU, RESET, RESET, RESET,
    RESET, RESET,  RESET, RESET, RESET,

                                              RESET,RESET,
                                                    RESET,
                                      RESET, RESET, RESET,
    // right hand
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, Y_ACU, U_ACU, I_ACU, O_ACU, RESET, RESET,
            RESET, RESET, RESET, L_ACU, RESET, RESET,
     RESET, N_ACU, RESET, RESET, RESET, RESET, RESET,
                   RESET, RESET, RESET, RESET, RESET,  
     RESET, RESET,
     RESET,
     RESET, RESET, RESET
),

/* Keymap 1: Grave layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |   è  |      |      |      |           |      |      |   ù  |   ì  |   ò  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |   à  |      |      |      |      |------|           |------|      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |  SHFT  |      |      |      |      |      |      |           |      |      |      |      |      |      |  SHFT  |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[GRAV] = LAYOUT_ergodox(
    // left hand
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, e_GRV, RESET, RESET, RESET,
    RESET, a_GRV,  RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, RESET, RESET,

                                              RESET,RESET,
                                                    RESET,
                                      RESET, RESET, RESET,
    // right hand
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, RESET, u_GRV, i_GRV, o_GRV, RESET, RESET,
            RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
                   RESET, RESET, RESET, RESET, RESET,  
     RESET, RESET,
     RESET,
     RESET, RESET, RESET
),

/* Keymap 1: Shifted Grave layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |   È  |      |      |      |           |      |      |   Ù  |   Ì  |   Ò  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |   À  |      |      |      |      |------|           |------|      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[S_GRAV] = LAYOUT_ergodox(
    // left hand
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, E_GRV, RESET, RESET, RESET,
    RESET, A_GRV,  RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, RESET, RESET,

                                              RESET,RESET,
                                                    RESET,
                                      RESET, RESET, RESET,
    // right hand
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, RESET, U_GRV, I_GRV, O_GRV, RESET, RESET,
            RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
                   RESET, RESET, RESET, RESET, RESET,  
     RESET, RESET,
     RESET,
     RESET, RESET, RESET
),

/* Keymap 1: Trema layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |   ë  |      |      |      |           |      |      |   ü  |   ï  |   ö  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |   ä  |      |      |      |      |------|           |------|      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |  SHFT  |      |      |      |      |      |      |           |      |      |      |      |      |      |  SHFT  |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[TREM] = LAYOUT_ergodox(
    // left hand
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, e_TRM, RESET, RESET, RESET,
    RESET, a_TRM,  RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, RESET, RESET,

                                              RESET,RESET,
                                                    RESET,
                                      RESET, RESET, RESET,
    // right hand
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, RESET, u_TRM, i_TRM, o_TRM, RESET, RESET,
            RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
                   RESET, RESET, RESET, RESET, RESET,  
     RESET, RESET,
     RESET,
     RESET, RESET, RESET
),

/* Keymap 1: Shifted Trema layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |   Ë  |      |      |      |           |      |      |   Ü  |   Ï  |   Ö  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |   Ä  |      |      |      |      |------|           |------|      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[S_TREM] = LAYOUT_ergodox(
    // left hand
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, E_TRM, RESET, RESET, RESET,
    RESET, A_TRM,  RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, RESET, RESET,

                                              RESET,RESET,
                                                    RESET,
                                      RESET, RESET, RESET,
    // right hand
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, RESET, U_TRM, I_TRM, O_TRM, RESET, RESET,
            RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
                   RESET, RESET, RESET, RESET, RESET,  
     RESET, RESET,
     RESET,
     RESET, RESET, RESET
),

/* Keymap 1: Breve layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |   ĕ  |      |      |      |           |      |      |   ŭ  |   ĭ  |   ŏ  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |   ă  |      |      |      |   ğ  |------|           |------|      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |  SHFT  |      |      |      |      |      |      |           |      |      |      |      |      |      |  SHFT  |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[BREV] = LAYOUT_ergodox(
    // left hand
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, e_BRV, RESET, RESET, RESET,
    RESET, a_BRV,  RESET, RESET, RESET, g_BRV,
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, RESET, RESET,

                                              RESET,RESET,
                                                    RESET,
                                      RESET, RESET, RESET,
    // right hand
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, RESET, u_BRV, i_BRV, o_BRV, RESET, RESET,
            RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
                   RESET, RESET, RESET, RESET, RESET,  
     RESET, RESET,
     RESET,
     RESET, RESET, RESET
),

/* Keymap 1: Shifted Breve layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |   Ĕ  |      |      |      |           |      |      |   Ŭ  |   Ĭ  |   Ŏ  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |   Ă  |      |      |      |   Ğ  |------|           |------|      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[S_BREV] = LAYOUT_ergodox(
    // left hand
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, E_BRV, RESET, RESET, RESET,
    RESET, A_BRV,  RESET, RESET, RESET, G_BRV,
    RESET, RESET,  RESET, RESET, RESET, RESET, RESET,
    RESET, RESET,  RESET, RESET, RESET,

                                              RESET,RESET,
                                                    RESET,
                                      RESET, RESET, RESET,
    // right hand
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, RESET, U_BRV, I_BRV, O_BRV, RESET, RESET,
            RESET, RESET, RESET, RESET, RESET, RESET,
     RESET, RESET, RESET, RESET, RESET, RESET, RESET,
                   RESET, RESET, RESET, RESET, RESET,  
     RESET, RESET,
     RESET,
     RESET, RESET, RESET
),

/* Keymap 2: Symbol Layer
 * 
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |   1  |   2  |   3  |   4  |   5  |      |           |      |   6  |   7  |   8  |   9  |   0  |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |   {  |   }  |      |      |           |      |   A  |   4  |   5  |   6  |   B  |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |   <  |   (  |   )  |   >  |------|           |------|   C  |   1  |   2  |   3  |   D  |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |  FUNC  |      |      |   [  |   ]  |      |      |           |      |   E  |   0  |   ,  |   .  |   F  |  FUNC  |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |   x  |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        | SLFT | SRGT |       | WLFT | WRGT |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      | WMAX |       | WCEN |      |      |
 *                                 `--------------------'       `--------------------'
 */
[SYMB] = LAYOUT_ergodox(
       // left hand
       KC_TRNS, KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_NO,  
       KC_TRNS, KC_NO,   KC_NO,   KC_LCBR, KC_RCBR, KC_NO,   KC_NO,  
       KC_TRNS, KC_NO,   KC_LT,   KC_LPRN, KC_RPRN, KC_GT,
       MO(FUN), KC_NO,   KC_NO,   KC_LBRC, KC_RBRC, KC_NO,   KC_NO,  
       KC_TRNS, KC_TRNS, KC_NO,   KC_NO,   KC_NO,  
                                         SCRLF, SCRRT,
                                                KC_TRNS,
                              KC_TRNS, KC_TRNS, WINMX,
       // right hand
       KC_NO,   KC_6,    KC_7, KC_8,    KC_9,    KC_0,    KC_TRNS,
       KC_NO,   S(KC_A), KC_4, KC_5,    KC_6,    S(KC_B), KC_NO,  
                S(KC_C), KC_1, KC_2,    KC_3,    S(KC_D), KC_TRNS,
       KC_NO,   S(KC_E), KC_0, KC_COMM, KC_DOT,  S(KC_F), MO(FUN),
                         KC_X, KC_NO,   KC_NO,   KC_NO,   KC_NO,  
       WINLF,   WINRT,
       KC_TRNS,
       WINCN,   KC_TRNS, KC_TRNS
),

/* Keymap 3: Function Keys
 * 
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |  F1  |  F2  |  F3  |  F4  |  F5  |      |           |      |  F6  |  F7  |  F8  |  F9  |  F10 |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |  F4  |  F5  |  F6  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |------|           |------|      |  F1  |  F2  |  F3  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |  F10 |  F11 |  F12 |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |  F13 |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[FUN] = LAYOUT_ergodox(
       // left hand
       KC_TRNS, KC_F1,   KC_F2, KC_F3, KC_F4, KC_F5, KC_NO,
       KC_TRNS, KC_NO,   KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,
       KC_TRNS, KC_NO,   KC_NO, KC_NO, KC_NO, KC_NO,
       KC_TRNS, KC_NO,   KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,
       KC_TRNS, KC_TRNS, KC_NO, KC_NO, KC_NO,  
                                         KC_NO, KC_NO,
                                                KC_NO,
                              KC_TRNS, KC_TRNS, KC_NO,
       // right hand
       KC_NO, KC_F6, KC_F7,  KC_F8,  KC_F9,  KC_F10, KC_NO,
       KC_NO, KC_NO, KC_F4,  KC_F5,  KC_F6,  KC_NO,  KC_NO,
              KC_NO, KC_F1,  KC_F2,  KC_F3,  KC_NO,  KC_TRNS,
       KC_NO, KC_NO, KC_F10, KC_F11, KC_F12, KC_NO,  KC_TRNS,
                     KC_X,   KC_NO,  KC_NO,  KC_NO,  KC_NO,
       KC_NO,   KC_NO,
       KC_NO,
       KC_NO,   KC_TRNS, KC_TRNS
),

/* Keymap 4: Game layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |  Tab   |   1  |   2  |   3  |   4  |   5  |  `~  |           |  Del |   6  |   7  |   8  |   9  |   0  |  Bksp  |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |  Esc   |   Q  |   W  |   E  |   R  |   T  |  +=  |           |  -_  |   Y  |   U  |   I  |   O  |   P  |   \|   |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |  MVMT  |   A  |   S  |   D  |   F  |   G  |------|           |------|   H  |   J  |   K  |   L  |  :;  |   '"   |
 * |--------+------+------+------+------+------|  [{  |           |  ]}  |------+------+------+------+------+--------|
 * |  Shft  |   Z  |   X  |   C  |   V  |   B  |      |           |      |   N  |   M  |  ,<  |  .>  |  /?  |  Shft  |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   | LCtl | LAlt |      |      |      |                                       |      |      |      | RAlt | RCtl |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        | Prev | Next |       | VolD | VolU |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      | Play |       | Mute |      |      |
 *                                 |  Spc | LCmd |------|       |------| RCmd | Spc  |
 *                                 |      |      | Base |       |      |  Ret |      |
 *                                 `--------------------'       `--------------------'
 */
[GAME] = LAYOUT_ergodox(
    // left hand
    KC_TAB,   KC_1,    KC_2,    KC_3,    KC_4,    KC_5, KC_GRV,
    KC_ESC,   KC_Q,    KC_W,    KC_E,    KC_R,    KC_T, KC_EQUAL,
    MO(MVMT), KC_A,    KC_S,    KC_D,    KC_F,    KC_G,
    KC_LSFT,  KC_Z,    KC_X,    KC_C,    KC_V,    KC_B, KC_LBRC,
    KC_LCTL,  KC_LALT, KC_NO,   KC_NO,   KC_NO,  

                                                 KC_MRWD, KC_MFFD,
                                                          KC_MPLY,
                                       KC_SPACE, KC_LGUI, TG(GAME),
    // right hand
     KC_DEL,  KC_6, KC_7,    KC_8,    KC_9,    KC_0,    KC_BSPC,
     KC_MINS, KC_Y, KC_U,    KC_I,    KC_O,    KC_P,    KC_BSLS,
              KC_H, KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT,
     KC_RBRC, KC_N, KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT,
                    KC_NO,   KC_NO,   KC_NO,   KC_RALT, KC_RCTL,
     KC_VOLD, KC_VOLU,
     KC_MUTE,
     KC_NO,   MT(MOD_RGUI, KC_ENT), KC_SPC
),

/* Keymap 5: Movement Layer
 * 
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      | home |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      | pgup | wlft |  up  | wrgt | pgdn |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |------|           |------| lsta | left | down | rght | lend |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |  end |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[MVMT] = LAYOUT_ergodox(
       // left hand
       KC_TRNS, KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,  
       KC_TRNS, KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,  
       KC_TRNS, KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,  
       KC_TRNS, KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,  
       KC_TRNS, KC_TRNS, KC_NO,   KC_NO,   KC_NO,  
                                         KC_NO,   KC_NO,  
                                                  KC_NO,  
                                KC_TRNS, KC_TRNS, KC_NO,  
       // right hand
       KC_NO,   KC_NO,   KC_NO,   KC_HOME, KC_NO,   KC_NO,   KC_TRNS,
       KC_NO,   KC_PGUP, WLEFT,   KC_UP,   WRGHT,   KC_PGDN, KC_TRNS,
                LSTRT,   KC_LEFT, KC_DOWN, KC_RGHT, LEND,    KC_TRNS,
       KC_NO,   KC_NO,   KC_NO,   KC_END,  KC_NO,   KC_NO,   KC_TRNS,
                         KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,  
       KC_NO,   KC_NO,  
       KC_NO,  
       KC_NO,   KC_TRNS, KC_TRNS
),
};

const uint16_t PROGMEM fn_actions[] = {
    [1] = ACTION_LAYER_TAP_TOGGLE(SYMB)                // FN1 - Momentary Layer 1 (Symbols)
};

const macro_t *action_get_macro(keyrecord_t *record, uint8_t id, uint8_t opt)
{
    // MACRODOWN only works in this function
    switch(id) {
    case EPRM:
        if (record->event.pressed) { // For resetting EEPROM
          eeconfig_init();
        }
        break;
    case RESET:
        reset_oneshot_layer();
        break;
    }

    return MACRO_NONE;
};

// Runs just one time when the keyboard initializes.
void matrix_init_user(void) {

};


// Runs constantly in the background, in a loop.
void matrix_scan_user(void) {

    uint8_t layer = biton32(layer_state);

    ergodox_board_led_off();
    ergodox_right_led_1_off();
    ergodox_right_led_2_off();
    ergodox_right_led_3_off();
    switch (layer) {
    case BASE:
        break;
    case SHFT:
        ergodox_right_led_1_on();
        break;
    case SYMB:
        ergodox_right_led_2_on();
        break;
    case MVMT:
        ergodox_right_led_3_on();
        break;
    case FUN:
        ergodox_right_led_1_on();
        ergodox_right_led_2_on();
        break;
    case CIRC:
        ergodox_right_led_1_on();
        ergodox_right_led_2_on();
        ergodox_right_led_3_on();
        break;
    case S_CIRC:
        ergodox_right_led_1_on();
        ergodox_right_led_3_on();
    default:
        // none
        break;
    }
};
